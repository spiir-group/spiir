# Template Waveform Generation

In this section we will go into further depth about the functionality provided by the
SPIIR package in relation to generation of compact binary coalescence data.

## Parameter Sampling

This section will discuss parameter sampling for compact binary coalescence simulation
models.

## Waveform Simulation

This section will discuss generating waveform polarisations using numerical relativity
simulations for compact binary coalescence.

## Interferometer Projection

This section will discuss waveform projection according to interferometer antenna beam
pattern functions and various extrinsic parameters (e.g. sky location, orientation).

## Bank Construction

This section will discuss the construction of template waveform banks to be used for
matched filtering.
