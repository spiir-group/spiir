#!/usr/bin/env python

import logging
from pathlib import Path
from typing import Optional, Sequence, Union

import click

from spiir.cli import click_logger_options
from spiir.logging import setup_logger
from spiir.analysis.far_contour import plot_far_contours


def split_paths(ctx, param, filepaths: Optional[str]) -> Optional[Sequence[Path]]:
    if filepaths:
        filepaths = [Path(fp) for fp in filepaths.split(',')]
    return filepaths

@click.command()
@click.argument("stats_path", type=click.Path())
@click.argument("ifos", type=str)
@click.argument("far_factor", type=float)
@click.argument("snr", type=float)
@click.argument("chisq", type=float)
@click.argument("output_path", type=click.Path())
@click.option("--zerolags-paths", type=str, help="Comma separted list of valid zerolag filepaths.", callback = split_paths, default=None)
@click.option("--snr-min", type=float, help="Min log snr for contours. Should correspond to snr bins in the stats file.", default=0.54)
@click.option("--snr-max", type=float, help="Max log snr for contours. Should correspond to snr bins in the stats file.", default=3.0)
@click.option("--chisq-min", type=float, help="Min log chisq for contours. Should correspond to chisq bins in the stats file.", default=-1.2)
@click.option("--chisq-max", type=float, help="Max log chisq for contours. Should correspond to chisq bins in the stats file.", default=3.5)
@click_logger_options
def main(stats_path: Union[str, Path],
    ifos: str,
    far_factor: float,
    snr: float,
    chisq: float,
    output_path: Union[str, Path],
    zerolags_paths: Optional[str] = None,
    snr_min: float = 0.54,
    snr_max: float = 3.0,
    chisq_min: float = -1.2,
    chisq_max: float = 3.5,
    log_level: int = logging.WARNING,
    log_file: Optional[Union[str, Path]] = None):
    setup_logger("spiir", log_level, log_file)

    fig = plot_far_contours(ifos, stats_path, far_factor, snr, chisq, zerolags_paths, snr_min, snr_max, chisq_min, chisq_max)

    fig.savefig(output_path)


if __name__ == "__main__":
    main()
