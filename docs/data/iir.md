# Infinite Impulse Response (IIR) Filters

This section will discuss what IIR filters are.

## Approximating Waveforms with IIR Filters

This section will discuss how to approximate waveform templates using IIR filters.
