"""Placeholder subpackage for SPIIR's Infinite Impulse Response filter approximations
for gravitational wave signal templates.

This code should be ported from the gstlal-spiir/python/spiirbank/ subpackage.
"""
