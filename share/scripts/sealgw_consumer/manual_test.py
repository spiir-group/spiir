import json
import logging

from sealgw import seal

import spiir.io.igwn

sealgw_consumer = spiir.io.igwn.alert.consumers.sealgw.SealGWAlertConsumer(
    seal_config='config_LHV_O4ExpectedPSD.json',
    models=seal.Seal,
    nlevel=5,
    nthreads=8,
    group = 'gracedb-playground',
)

# load an example payload dictionary saved to disk as json
payload_path='payload_T905508.json'

with open(payload_path) as f:
    payload = json.load(f)

print("\nTesting: No local coinc, no fudge factor applied")
sealgw_consumer.process_alert(
    payload=payload,
    coinc_path=None,
    save_fits=True,
    save_png=True,
    fudge_factor=-1
)


# Applying fudge factor takes ~0.1-0.2s.
# If we do want to use it, may need to rewrite it in C to speed it up.
fudge_factor = 0.95

# coinc.xml is used to retrieve SNR Series without routing through GraceDb
coinc_path='coinc_T905508.xml'

print(
    "\nTesting: Has a local coinc, "
    f"fudge factor = {fudge_factor} applied. "
    f"{fudge_factor} is from O4 BNS injection tests."
)

sealgw_consumer.process_alert(
    payload=payload,
    coinc_path=coinc_path,
    save_fits=True,
    save_png=False,
    fudge_factor=fudge_factor
)

sealgw_consumer.close()
